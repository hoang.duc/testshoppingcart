package controllers

import (
	"myapp/database"
	"myapp/models/cart"
	"myapp/models/shoe"
	"myapp/models/user"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type TemplateRepo struct {
	DB *gorm.DB
}

func New() *TemplateRepo {
	db := database.InitDb()
	db.AutoMigrate(&user.User{})
	db.AutoMigrate(&shoe.Shoe{})
	db.AutoMigrate(&cart.Cart{})
	return &TemplateRepo{DB: db}
}

// //-------------------------- Create USER --------------------------------
// Vi du request JSON
//
//	{
//	    "name": "duc",
//	    "email":"gi@gmail.com",
//	    "password":"hom nay la",
//	    "image":"hhhh"
//	}
func (repository *TemplateRepo) CreateUsers(c *gin.Context) {
	var data map[string]string
	c.BindJSON(&data)
	password, _ := bcrypt.GenerateFromPassword([]byte(data["password"]), 14)
	if (strings.Contains(data["email"], "@gmail.com") == true) || (strings.Contains(data["email"], "@hcmut.edu.vn") == true) {

		user := user.User{
			Name:     data["name"],
			Email:    data["email"],
			Image:    data["image"],
			Password: password,
		}
		//user.CreateUser(repository.DB, &user)
		err := repository.DB.Create(&user).Error

		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
			return
		}
		c.JSON(http.StatusOK, gin.H{"data": "đăng ký tài khoản thành công"})
	} else {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "email người dùng nhập sai"})
		return
	}
}

// //-------------------------- CRUD SHOE --------------------------------
func (repository *TemplateRepo) CreateShoe(c *gin.Context) {
	var dataJson shoe.Shoe
	c.ShouldBind(&dataJson)

	err := repository.DB.Model(&shoe.Shoe{}).Create(map[string]interface{}{
		"Name":        dataJson.Name,
		"Description": dataJson.Description,
		"Image":       dataJson.Image,
		"Color":       dataJson.Color,
		"Price":       dataJson.Price,
		"CreatedAt":   time.Now(),
	}).Error
	if err != nil {
		println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	var result shoe.Shoe
	repository.DB.Last(&result)
	c.JSON(http.StatusOK, gin.H{"data": result})
}

func (repository *TemplateRepo) ReadAllShoe(c *gin.Context) {
	var shoes []shoe.Shoe

	err := repository.DB.Model(shoe.Shoe{}).Find(&shoes).Error
	if err != nil {
		println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": shoes})
}

func (repository *TemplateRepo) ReadShoe(c *gin.Context) {
	shoeId, _ := strconv.Atoi(c.Param("id_shoe"))
	var shoe shoe.Shoe
	err := repository.DB.First(&shoe, shoeId).Error
	if err != nil {
		println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": shoe})
}

func (repository *TemplateRepo) UpdateShoe(c *gin.Context) {
	id, err := strconv.Atoi(c.Params.ByName("id"))
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"code": 1, "message": err.Error(), "data": nil})
		return
	}
	var shoe shoe.Shoe

	if err := repository.DB.Where("id = ?", id).Table("shoes").First(&shoe).Error; err != nil {
		c.AbortWithStatusJSON(400, gin.H{"code": 1, "message": err.Error(), "data": nil})
		return
	}
	c.ShouldBind(&shoe)
	repository.DB.Save(&shoe)

	c.AbortWithStatusJSON(200, gin.H{"code": 0, "message": "success", "data": shoe})
}

func (repository *TemplateRepo) DeleteShoe(c *gin.Context) {
	id, err := strconv.Atoi(c.Params.ByName("id"))
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"code": 1, "message": err.Error(), "data": nil})
		return
	}
	var shoe shoe.Shoe

	if err := repository.DB.Where("id = ?", id).Delete(&shoe).Error; err != nil {
		c.AbortWithStatusJSON(400, gin.H{"code": 1, "message": err.Error(), "data": nil})
		return
	}

	c.AbortWithStatusJSON(200, gin.H{"code": 0, "message": "success"})
}

// //-------------------------- ADD CART --------------------------------

func (repository *TemplateRepo) AddShoeInCart(c *gin.Context) {
	shoeId := c.Query("id_shoe")
	userId := c.Query("id_user")
	// VÌ ID CỦA USER SẼ LẤY TỪ TOKEN NHƯNG Ở ĐÂY ĐỂ NHANH GỌN CHÚNG TA SẼ TẠM THỜI LẤY ID CỦA USER TỪ REQUEST POSTMAN LUÔN
	var datashoe shoe.Shoe
	if err := repository.DB.First(&datashoe, shoeId).Error; err != nil {
		c.AbortWithStatusJSON(400, gin.H{"code": 1, "message": err.Error(), "data": nil})
		return
	}
	err := repository.DB.Model(&cart.Cart{}).Create(map[string]interface{}{
		"IdShoe":     shoeId,
		"IdUser":     userId,
		"AmountShoe": 1,
		"Total":      datashoe.Price,
		"CreatedAt":  time.Now(),
	}).Error
	if err != nil {
		println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	var result cart.Cart
	repository.DB.Last(&result)
	c.JSON(http.StatusOK, gin.H{"data": result})
}

func (repository *TemplateRepo) GetALLCart(c *gin.Context) {
	userId, _ := strconv.Atoi(c.Param("id"))
	var carts []cart.Cart
	err := repository.DB.Where("id_user = ?", userId).Find(&carts).Error
	if err != nil {
		println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": carts})
}

func (repository *TemplateRepo) ChangeAmountShoeInCart(c *gin.Context) {
	shoeId := c.Query("id_shoe")
	userId := c.Query("id_user")
	var cart cart.Cart
	c.ShouldBind(&cart)
	repository.DB.Model(&cart).Where("id_shoe = ? AND id_user = ?", shoeId, userId).Update("amount_shoe", cart.AmountShoe)

	if err := repository.DB.Where("id_shoe = ?", shoeId).Find(&cart).Error; err != nil {
		c.AbortWithStatusJSON(400, gin.H{"code": 1, "message": err.Error(), "data": nil})
		return
	}
	c.AbortWithStatusJSON(200, gin.H{"code": 0, "message": "success", "data": cart})
}

func (repository *TemplateRepo) PaymentCart(c *gin.Context) {
	userId, _ := strconv.Atoi(c.Param("id"))
	var carts []cart.Cart
	err := repository.DB.Where("id_user = ?", userId).Find(&carts).Error
	if err != nil {
		println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	payment := 0
	for i := 0; i < len(carts); i++ {
		payment = payment + carts[i].AmountShoe*carts[i].Total
	}
	c.JSON(http.StatusOK, gin.H{"data": payment})
}
