package main

import (
	controllers "myapp/controllers/controller"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	r := setupRouter()
	_ = r.Run(":8080")
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(cors.Default())
	r.GET("ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, "pong")
	})

	controllers := controllers.New()
	r.POST("user/register", controllers.CreateUsers)
	r.POST("shoe/create_shoe", controllers.CreateShoe)
	r.GET("shoe/read_all_shoe", controllers.ReadAllShoe)
	r.GET("shoe/read_all_shoe/:id_shoe", controllers.ReadShoe)
	r.PUT("shoe/update_shoe/:id", controllers.UpdateShoe)
	r.DELETE("shoe/delete_shoe/:id", controllers.DeleteShoe)
	r.POST("cart/add_shoe_in_cart", controllers.AddShoeInCart)
	r.GET("cart/get_all_cart/:id", controllers.GetALLCart)
	r.PUT("cart/change_amount_shoe", controllers.ChangeAmountShoeInCart)
	r.GET("cart/payment/:id", controllers.PaymentCart)
	return r
}
