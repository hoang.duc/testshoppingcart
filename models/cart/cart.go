package cart

import (
	"time"

	"gorm.io/gorm"
)

type Cart struct {
	Id         uint           `json:"id"`
	IdShoe     uint           `json:"shoe"`
	IdUser     uint           `json:"id_user" gorm:"not null"`
	AmountShoe int            `json:"amount" gorm:"not null"`
	Total      int            `json:"total" gorm:"not null"`
	CreatedAt  time.Time      `json:"created_at,omitempty" gorm:"not null"`
	UpdatedAt  *time.Time     `json:"updated_at,omitempty"`
	DeletedAt  gorm.DeletedAt `json:"deleted_at,omitempty"`
}
