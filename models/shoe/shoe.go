package shoe

import (
	"myapp/models/cart"
	"time"

	"gorm.io/gorm"
)

type Shoe struct {
	Id          uint           `json:"id" gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	Image       string         `json:"image" gorm:"not null"`
	Name        string         `json:"name" gorm:"not null"`
	Description string         `json:"description" gorm:"not null"`
	Price       int            `json:"price" gorm:"not null"`
	Color       string         `json:"color" gorm:"not null"`
	Cart        []cart.Cart    `json:"cart,omitempty" gorm:"foreignKey:IdShoe" `
	CreatedAt   time.Time      `json:"created_at,omitempty" gorm:"not null"`
	UpdatedAt   *time.Time     `json:"updated_at,omitempty"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at,omitempty"`
}
