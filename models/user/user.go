package user

import (
	"myapp/models/cart"
	"time"

	"gorm.io/gorm"
)

type User struct {
	Id        uint           `json:"id" gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	Image     string         `json:"image" gorm:"not null"`
	Name      string         `json:"name" gorm:"not null"`
	Email     string         `json:"email" gorm:"not null"`
	Password  []byte         `json:"-"`
	Cart      []cart.Cart    `json:"cart,omitempty" gorm:"foreignKey:IdUser"`
	CreatedAt time.Time      `json:"created_at,omitempty" gorm:"not null"`
	UpdatedAt *time.Time     `json:"updated_at,omitempty"`
	DeletedAt gorm.DeletedAt `json:"deleted_at,omitempty" gorm:"index"`
}
